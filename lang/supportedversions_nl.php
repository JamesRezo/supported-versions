<?php

return [
	'branch' => 'Branch' ,
	'active_support' => 'Actieve ondersteuning',
	'active_support_definition' => 'Een release die actief wordt ondersteund. Gerapporteerde fouten en beveligingsproblemen worden gecorrigeerd en regelmatig gereleased.',
	'active_support_until' => 'Actieve ondersteuning tot',
	'security_fix' => 'Uitsluitende beveiligingscorrecties',
	'security_fix_definition' => 'Een release die alleen voor kritische beveiligingsproblemen wordt ondersteund. Releases uitsluitend indien noodzakelijk.',
	'security_support_until' => 'Beveligingsondersteuning tot',
	'end_of_life' => 'Einde Leven',
	'end_of_life_definition' => 'Een release die niet langer wordt ondersteund. Gebruikers van deze release moeten zo snel mogelijk upgraden, omdat zij risico lopen op niet-gecorrigeerde beveiligingsproblemen.',
	'initial_release' => 'Initiële Release',
	'unreleased' => 'Niet-gereleasde versie',
	'unreleased_definition' => 'Een niet geplande release.',
	'php_compatibility' => 'PHP Compatibiliteit',
];
