<?php

return [
	'branch' => 'Branch',
	'active_support' => 'Active support',
	'active_support_definition' => 'A release that is being actively supported.
		Reported bugs and security issues are fixed and regular point releases are made.',
	'active_support_until' => 'Active Support Until',
	'security_fix' => 'Security fixes only',
	'security_fix_definition' => 'A release that is supported for critical security issues only.
		Releases are only made on an as-needed basis.',
	'security_support_until' => 'Security Support Until',
	'end_of_life' => 'End of life',
	'end_of_life_definition' => 'A release that is no longer supported.
		Users of this release should upgrade as soon as possible,
		as they may be exposed to unpatched security vulnerabilities.',
	'initial_release' => 'Initial Release',
	'unreleased' => 'Unreleased version',
	'unreleased_definition' => 'A release that is not planned.',
	'php_compatibility' => 'PHP Compatibility',
	'last_release' => 'Last release',
	'latest_releases' => 'Latest releases',
	'current_page' => 'current version',
	'eol_page' => 'A table of end of life branches is available.',
	'released_at' => 'Released',
	'announcement' => 'Announcement',
	'changelog' => 'Changelog',
	'download' => 'Download',
	'download_size' => 'Size',
	'freespace' => 'Free space (off database)',
	'ram' => 'RAM',
	'system_needs'  => 'System needs',
	'sql' => 'Database',
	'image_processing' => 'Image Processing',
	'required' => 'Required',
	'suggest' => 'Suggestions',
	'provided' => 'Provided',
	'php_extensions' => 'PHP Extensions',
	'no_future_version' => 'No future version planned.',
	'no_maintained_version' => 'No maintained version.',
];
