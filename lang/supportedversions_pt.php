<?php

return [
	'branch' => 'Ramo',
	'active_support' => 'Manutenção ativa',
	'active_support_definition' => 'Uma versão com suporte ativo.
        Os bugs assinalados e os problemas de segurança são corrigidos
        e as atualizações regulares são realizadas.',
	'active_support_until' => 'Manutenção ativa até',
	'security_fix' => 'Apenas correções de segurança',
	'security_fix_definition' => 'Uma versão suportada apenas para problemas
		críticos de segurança.
		As atualizações são exclusivamente realizadas de acordo com a necessidade.',
	'security_support_until' => 'Correções de segurança até',
	'end_of_life' => 'Fim de vida',
	'end_of_life_definition' => 'Uma versão que não é mais suportada.
		As pessoas qque utilizam esta versão devem atualizar assim que possível,
		pois podem ficar expostas a vulnerabilidades de segurança
		Não corrigidas.',
	'initial_release' => 'Primeira publicação',
	'unreleased' => 'Não publicada',
	'unreleased_definition' => 'Uma versão que ainda não foi publicada.',
	'php_compatibility' => 'Compatibilidade PHP',
	'last_release' => 'Última publicação',
	'latest_releases' => 'Últimas versões',
	'current_page' => 'versão mantida',
	'eol_page' => 'Uma tabela de fim de vida dos ramos está disponível.',
	'released_at' => 'Publicada em',
	'announcement' => 'Anúncio',
	'changelog' => 'Changelog',
	'download' => 'Download',
	'download_size' => 'Tamanho',
	'freespace' => 'Espaço em disco (sem a base de dados)',
	'ram' => 'Memória RAM',
	'system_needs'  => 'Requisitos mínimos do sistema',
	'sql' => 'Base de dados',
	'image_processing' => 'Tratamento de imagens',
	'required' => 'Requerido',
	'suggest' => 'Sugestões',
	'provided' => 'Fornecido',
	'php_extensions' => 'Extensões PHP',
	'no_future_version' => 'Nenhuma versão futura planeada atualmente',
	'no_maintained_version' => 'Nenhuma versão mantida atualmente.',
];
